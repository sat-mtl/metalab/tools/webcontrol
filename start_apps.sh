#!/bin/bash

# Function to check if an application is running
is_running() {
    pgrep -x "$1" > /dev/null
}

# Function to check if an application is running as a Flatpak
is_flatpak_running() {
    flatpak ps | grep "$1" > /dev/null
}

# Start QjackCtl if not running
if ! is_running "qjackctl"; then
    qjackctl &
fi

# Wait for QjackCtl to start
sleep 5

# Start Reaper (Flatpak) if not running
if ! is_flatpak_running "org.cockos.Reaper"; then
    flatpak run fm.reaper.Reaper &
fi

# Wait for Reaper to start
sleep 5

# Start your Python app if not running
if ! is_running "python" || ! pgrep -f "app.py" > /dev/null; then
    python app.py &
fi
