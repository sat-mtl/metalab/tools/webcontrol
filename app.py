from flask import Flask, render_template, request
import rtmidi
from pythonosc import udp_client
import subprocess

app = Flask(__name__)

# Configure the OSC client
osc_client = udp_client.SimpleUDPClient("127.0.0.1", 9000)  # Adjust the IP and port as needed

# Create a virtual MIDI output port
midiout = rtmidi.RtMidiOut()
virtual_port_name = "Webcontrol"
midiout.openVirtualPort(virtual_port_name)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/send_message', methods=['POST'])
def send_message():
    volume = float(request.form['volume'])

    midi_cc_message = rtmidi.MidiMessage.controllerEvent(0, 16, int(volume*127))
    midiout.sendMessage(midi_cc_message)

    osc_client.send_message("/master/volume", volume)

    return 'Message sent'

@app.route('/send_play', methods=['POST'])
def send_play():
    try:
        # Send OSC message to start playing
        osc_client.send_message("/play", 1)
        return '', 204
    except Exception as e:
        return str(e), 500

@app.route('/send_pause', methods=['POST'])
def send_pause():
    try:
        # Send OSC message to pause
        osc_client.send_message("/pause", 1)
        return '', 204
    except Exception as e:
        return str(e), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
