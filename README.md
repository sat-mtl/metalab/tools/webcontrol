# Web remote control for Reaper

Controls basic Reaper options (master volume) over the web browser

## Custom python control

Install requirements using `pip install -r requirements.txt` (recommended creating a python venv).

Run `python app.py`

### Setting Python code to run automatically on boot

Do not forget to change the username and path to the cloned folder

```bash
cat <<- "EOF" | sudo tee /lib/systemd/system/webcontrol.service
[Unit]
Description=Web Control for Reaper
After=multi-user.target

[Service]
User=metalab
Type=idle
ExecStart=python /webcontrol/app.py

[Install]
WantedBy=multi-user.target
EOF
sudo systemctl daemon-reload
sudo systemctl enable --now webcontrol.service
```

## Using the built-in control

This approach is not recommended if the user is not familiar with Reaper as it gives access to the record button for each track, mix control, etc.

- Open Reaper
- Enter Options -> Preferences
- Go to Control/OSC/web
- Add a new control
- Choose `web browser interface`
- Choose the port (e.g., 9000)
- Choose the interface
- Hit `Apply settings`
- Save the project
